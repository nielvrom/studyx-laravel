<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['title', 'body', 'excerpt', 'published_at'];

    public function setPublishedAtAttribute($date) //setFieldAttribute
    {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    public function setPasswordAttribute($data)
    {
        $this->attributes['password'] = bcrypt($data);
    }

    public function setTitleAttribute($data)
    {
        $this->attributes['title'] = 'Laravel - ' . $data;
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeUnpublished($query)
    {
        return $query->where('published_at', '>', Carbon::now());
    }

    public function user()
    {
        $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

}
