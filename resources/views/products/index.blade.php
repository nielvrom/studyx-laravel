@extends('app')

@section('content')
    <h1>Overzicht producten</h1>
    @if(count($products) > 0)
        @foreach($products as $product)
            <a href="{{ action('ProductsController@show', [$product->id]) }}">{{ $product->title }}</a>
            <p>{{ $product->body }}</p>
            <p>{{ $product->excerpt }}</p>
            <a href="{{ action('ProductsController@destroy', [$product->id]) }}">Delete Product</a>
            <hr />
        @endforeach
    @else
        <p>Er zijn nog geen producten beschikbaar.</p>
    @endif

    <a href="/products/create">Maak nieuw product aan.</a>

@stop