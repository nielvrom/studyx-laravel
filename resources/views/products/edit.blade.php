@extends('app')

@section('content')
    <h1>Product aanpassen</h1>

    {!! Form::model($article, ['method' => 'PATCH', 'url' => 'products/' . $product->id]) !!}

        @include('articles._form', ['buttontext' => 'Aanpassen'])

    {!! Form::close() !!}

@stop