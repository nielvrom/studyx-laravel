@extends('app')

@section('content')
    <h1>Update artikel met id {{ $article->id }}</h1>

    {!! Form::model($article, ['method' => 'PATCH', 'url' => 'articles/' . $article->id]) !!}

        @include('articles._form', ['buttontext' => 'Wijzigen'])

    {!! Form::close() !!}

@stop