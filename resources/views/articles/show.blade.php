@extends('app')

@section('content')
    <h1>{{ $article->title }}</h1>
    <p>{{ $article->body }}</p>
    <p>Tags:</p>
    @foreach($article->tags as $tag)
        <span>{{ $tag->name }}</span>
    @endforeach

    <hr />
@stop