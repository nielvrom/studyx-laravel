@extends('app')

@section('content')
    <h1>Maak artikel aan</h1>

    {!! Form::open(['url' => 'articles']) !!}

        @include('articles._form', ['buttontext' => 'Create'])

    {!! Form::close() !!}

@stop