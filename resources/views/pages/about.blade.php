@extends('app')


@section('content')
    <h1>Meer over ons</h1>
    @if(count($people) > 0)
        @foreach($people as $person)
            <p>{!! $person !!}</p>
        @endforeach
    @endif
@stop

