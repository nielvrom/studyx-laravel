- composer install
- Database aanmaken
- .env.example kopiëren naar .env
- database parameters aanpassen in .env
- virtualhost instellen (public)
- php artian key:generate (key invullen in .env)
- php artisan migrate (database genereren)

optioneel: seeds / faker